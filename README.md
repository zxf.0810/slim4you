# slim4you
> slim4 框架 demo

### 背景

PHP有很多框架，比如Laravel，Symfony，国内的ThinkPHP等，要么框架本身太过繁杂和笨重，要么更新不是很及时，我们都知道java的spring系列框架，尤其是注解、依赖注入、控制反转等特性，非常受欢迎，那PHP到底有没有类似框架呢？


### 介绍

Slim4框架，它支持PHP7，注解，依赖注入和控制反转，而且你可以自由组合和配置框架，只需要安装你需要的组件即可。

### 框架初始目录

- public
    - index.php
- vendor
- composer.json    

### 安装教程

#### 只安装slim核心代码

$ mkdir -p slim-demo/public && cd slim-demo

$ composer require slim/slim:"4.*" slim/psr7

$ touch public/index.php //复制页面最上面的demo代码

$ php -S localhost:8080 -t public public/index.php


#### 使用Slim-Skeleton

> Slim-Skeleton是slim作者基于slim开发的基础骨架，也是作者的最佳开发实践。

开始使用slim最简单的方法是用Slim-Skeleton创建工程项目，只需要执行如下命令：

$ php composer.phar create-project slim/slim-skeleton:dev-master [my-app-name]

用你的项目名称替换 [my-app-name]这里。


#### 你可以使用PHP内置web服务器运行项目：

$ cd [my-app-name]; php -S localhost:8080 -t public public/index.php
